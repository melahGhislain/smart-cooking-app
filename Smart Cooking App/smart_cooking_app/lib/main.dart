import 'package:flutter/material.dart';
import 'package:smart_cooking_app/screens/homeScreen.dart';

void main() {
  runApp(SmartCooking());
}

class SmartCooking extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of the app i.e the main color
        primarySwatch: Colors.pink,
      ),
      home: HomeScreen(),
      // routes: {
      //   "/": (context)=> WelcomeScreen(),
      //   "/home": (context)=> HomeScreen(),
      //   "/input": (context)=> InputScreen(),
      //   "/detail": (context)=> DetailScreen(),
      //
      // },
    );
  }
}
