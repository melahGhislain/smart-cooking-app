import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_cooking_app/models/foodModel.dart';
import 'package:smart_cooking_app/screens/detailScreen.dart';

class FoodCard extends StatelessWidget {
  final FoodModel food;
  FoodCard(this.food, {Key key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailScreen(food)));
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.black87,
          ),
        margin: EdgeInsets.only(bottom: 10),
        //height: 200,
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //title
                    Text(
                      food.title.length > 15 ? food.title.substring(0, 15) : food.title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Chip(
                      backgroundColor: Colors.black54,
                      avatar: Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                        label: Text("${food.likes}", style: TextStyle(color: Colors.white),),
                    ),
                     ElevatedButton(
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => DetailScreen(food)));
                        },
                        child: Text("Details"),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                children: [
                  Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)),
                      child: Image(
                        image: NetworkImage(food.image),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  List<Widget> stars(int num){
    List icons = [];
    for(int i = 0; i < num; i++){
      icons.add(Icon(Icons.star, color: Colors.pink, size: 16,));
    }
    return icons;
  }
}

