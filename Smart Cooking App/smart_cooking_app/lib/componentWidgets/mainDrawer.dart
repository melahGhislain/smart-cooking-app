import 'package:flutter/material.dart';
import 'package:smart_cooking_app/screens/helpScreen.dart';
import 'package:smart_cooking_app/screens/homeScreen.dart';
import 'package:smart_cooking_app/screens/inputScreen.dart';
import 'package:smart_cooking_app/screens/itemScreen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.pinkAccent, Colors.pink[300]],
                begin: Alignment.center,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Center(
              child: Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.all(15),
                child: Column(

                  children: [
                    CircleAvatar(
                      radius: 70,
                      backgroundImage: AssetImage("assets/veg-1.png"),
                    ),
                    SizedBox(height: 10,),
                    Text(
                      "Smart Cook",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Card(
              child: ListTile(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                leading: Icon(
                  Icons.home,
                  size: 30,
                  color: Colors.blueGrey,
                ),
                title: Text(
                  "Home",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
          ),

          // input ingredient navigation bar
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Card(
              child: ListTile(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => InputScreen()));
                },
                leading: Icon(
                  Icons.list_alt,
                  size: 30,
                  color: Colors.blueGrey,
                ),
                title: Text(
                  "Input Ingredient",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
          ),
          // About us navigation bar
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Card(
              child: ListTile(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Help()));
                },
                leading: Icon(
                  Icons.person,
                  size: 30,
                  color: Colors.blueGrey,
                ),
                title: Text(
                  "Help",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

}
