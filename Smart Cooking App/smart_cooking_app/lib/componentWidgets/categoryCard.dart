import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_cooking_app/models/category.dart';
import 'package:smart_cooking_app/screens/itemScreen.dart';

// custom card widget that takes in two input of type string
// ie the image url and the image description

class CategoryCard extends StatelessWidget {
  final Category category;
  CategoryCard(this.category,{Key key}):super(key: key);
  final double imageSize = 90;

  //text style
  final TextStyle _textStyle = TextStyle(
    fontWeight: FontWeight.bold,
  );


  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => Items(list: ["meat", "rice"],)));
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        width: 110,
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Column(
            children: [
              Image(
                image: AssetImage(category.imageUrl),
                height: imageSize,
                width: imageSize,
              ),
              Text(
                category.imageTitle,
                style: _textStyle,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
