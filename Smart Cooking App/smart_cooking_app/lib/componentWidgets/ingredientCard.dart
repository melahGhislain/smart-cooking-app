import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_cooking_app/models/foodModel.dart';


class IngredientCard extends StatelessWidget {
  final SedIngredient food;
  IngredientCard(this.food,{Key key}):super(key: key);
  //final double imageSize = 20;

  //text style
  final TextStyle _textStyle = TextStyle(
    fontWeight: FontWeight.bold,
  );




  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
      width: 110,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        child: Column(
          children: [
            Image(
              image: AssetImage(food.image),
              // height: imageSize,
              // width: imageSize,
            ),
            Text(
              food.name,
              style: _textStyle,
            ),
          ],
        ),
      ),
    );
  }
}
