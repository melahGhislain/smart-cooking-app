import 'package:flutter/material.dart';

class Ingredient extends StatelessWidget {
  final String ingredient;
  final Function delete;
  Ingredient({ this.ingredient, this.delete ,Key key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 10,
                        child: Icon(Icons.done,
                          color: Colors.white,
                          size: 15,
                        ),
                      ),
                      SizedBox(width: 10,),
                      Text(
                        ingredient,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      )
                    ],
                  )
                ],
              ),
              Column(
                children: [
                    IconButton(
                        icon: Icon(
                            Icons.delete,
                        ),
                        onPressed: delete,
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
