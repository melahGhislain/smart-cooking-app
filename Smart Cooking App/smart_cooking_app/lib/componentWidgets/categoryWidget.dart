import 'package:flutter/material.dart';
import 'package:smart_cooking_app/models/category.dart';

class CategoryWidget extends StatelessWidget {
 final Category category;
  CategoryWidget(this.category,{Key key}):super(key: key);

  final double imageSize = 50;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
      width: 180,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CircleAvatar(
              backgroundColor: Colors.grey,
              radius: imageSize/2,
              child: Image(
                image: AssetImage(category.imageUrl),
                height: imageSize,
                width: imageSize,
              ),
            ),
            Text(
              category.imageTitle,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
