import 'package:flutter/material.dart';
import 'package:smart_cooking_app/componentWidgets/categoryWidget.dart';
import 'package:smart_cooking_app/componentWidgets/foodCard.dart';
import 'package:smart_cooking_app/componentWidgets/mainDrawer.dart';
import 'package:smart_cooking_app/controller/data.dart';
import 'package:smart_cooking_app/models/foodModel.dart';

class Items extends StatefulWidget {
  final List<String> list;
  Items({this.list});

  @override
  _ItemsState createState() => _ItemsState();
}

class _ItemsState extends State<Items> {




  Future<List<FoodModel>>  _foodModel;
  @override
  void initState() {
    super.initState();
    String ingredient = getString(widget.list);
    _foodModel = Data(ingredients: ingredient).getFood();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      endDrawer: MainDrawer(),
      appBar: AppBar(
        title: FutureBuilder(
              future: _foodModel,
              builder: (context, snapshot){
                if(snapshot.hasData){
                  return  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        // this should not be hard coded
                        backgroundImage: NetworkImage(snapshot.data[0].image),
                      ),
                      SizedBox(width: 8,),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Category"),
                          Text(
                            snapshot.data[0].title.length > 30 ? snapshot.data[0].title.substring(0,30) : snapshot.data[0].title,
                            style: TextStyle(
                              fontSize: 10,
                            ),),
                        ],
                      )
                    ],
                  );
                }else{
                  return Center(
                    child: Text("Loading..."),
                  );
                }

              },
            ),

        leading: IconButton(
            icon: Icon(Icons.arrow_back_rounded),
            onPressed: ()=> Navigator.of(context).pop(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10,10,10,0),
        child: FutureBuilder<List<FoodModel>>(
          future: _foodModel,
          builder: (context, snapshot){
            if(snapshot.hasData){
              return  ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index){
                  return FoodCard(snapshot.data[index]);
                },
              );
            }else{
              return Center(
                child: CircularProgressIndicator(),
              );
            }

          },
        ),
      ),

    );
  }
  // creates the string that can be passed to api url
  String getString(List<String> data){
    String temp = "${data[0]}";
    for(int i = 1; i < data.length; i++){
      temp += ",+${data[i]}";
    }
    return temp;
  }

}
