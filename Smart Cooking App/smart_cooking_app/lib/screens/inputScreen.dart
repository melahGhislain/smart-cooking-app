import 'package:flutter/material.dart';
import 'package:smart_cooking_app/componentWidgets/singleIngredient.dart';
import 'package:smart_cooking_app/screens/itemScreen.dart';

class InputScreen extends StatefulWidget {
  @override
  _InputScreenState createState() => _InputScreenState();
}

class _InputScreenState extends State<InputScreen> {
  // Question how can i uniquely identify an ingredient each ingredient needs a key
  TextEditingController _controller;
  List<String> ingredient = [];
  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[100],
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          " Enter Ingredients",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        actions: [
          FlatButton(
            textColor: Colors.white60,
            onPressed: (){
              setState(() {
                ingredient = [];
              });
            },
            child: Text("CLEAR"),
          ),

          IconButton(
            icon: Icon(
              Icons.send,
              color: Colors.white,
            ),
            onPressed: (){
              if(ingredient.isNotEmpty){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Items(list: ingredient)));
              }
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Stack(
          children: [
            Container(
              child: ListView(
                children: ingredient.map((String val) {
                  //print(index);
                  return Ingredient(
                    ingredient: val,
                    delete: (){
                      setState(() {
                        ingredient.remove(val);
                      });
                    },);
                }).toList(),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width-75,
                    height: 50,
                    child: TextField(
                      textAlignVertical: TextAlignVertical.center,
                      controller: _controller,
                      decoration: InputDecoration(
                        hintText: "Enter Ingredient",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(
                            color: Colors.grey[100],
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                  ),

                  CircleAvatar(
                    radius: 28,
                    child: IconButton(
                      icon: Icon(
                        Icons.add,
                        size: 25,
                      ),
                      color: Colors.white,
                      onPressed: (){
                        addIngredient();
                      },
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void addIngredient(){
    String textInput = _controller.text;
    if(textInput.trim() != ""){
      setState(() {

        ingredient.add(textInput.trim());
        _controller.text = "";
      });
    }
  }

}
