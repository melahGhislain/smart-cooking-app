import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:smart_cooking_app/componentWidgets/categoryCard.dart';
import 'package:smart_cooking_app/componentWidgets/foodCard.dart';
import 'package:smart_cooking_app/componentWidgets/mainDrawer.dart';
import 'package:smart_cooking_app/controller/data.dart';
import 'package:smart_cooking_app/models/category.dart';
import 'package:smart_cooking_app/models/food.dart';
import 'package:smart_cooking_app/models/foodModel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double _height = 8;
  double _padding = 10;
  TextEditingController _controller = TextEditingController();
  List<Category> temp;
  List item;
  List<Category> _categories = <Category>[
    Category(imageUrl: "assets/chicken.png", imageTitle: "Fried ",),
    Category(imageUrl: "assets/chicken1.png", imageTitle: "Chicken",),
    Category(imageUrl: "assets/chicken.png", imageTitle: "Rice",),
    Category(imageUrl: "assets/veg-1.png", imageTitle: "Vegetable",),
  ];

  Future<List<FoodModel>>  _foodModel;


  @override
  void initState() {
    super.initState();
    temp = _categories;
    item = [];
    _foodModel = Data(ingredients: "meat,+rice,+tomatoes").getFood();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Smart Cooking"),
      ),
      body: ListView(
        children: [
          Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(_padding, _padding, _padding, 0),
          child: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "Smart App",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: _height,),
              // Search input form field
              Container(
                height: 50,
                child: TextField(
                  onChanged: (String val){
                    filterCategory(val.trim());
                  },
                  controller: _controller,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.search),
                    hintText: "Search",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
              ),
              SizedBox(height: _height,),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "Categories",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: _height,),
             // List view builder that displays the different categories
              Container(
                height: 150,
                child: FutureBuilder<List<FoodModel>>(
                  future: _foodModel,
                  builder: (context, snapshot){
                    if(snapshot.hasData){
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: _categories.length,
                        itemBuilder: (BuildContext context, int index){
                          return CategoryCard(_categories[index]);
                        },
                      );
                    }else{
                      return Center(child: CircularProgressIndicator());
                    }

                  },
                ),
              ),
              SizedBox(height: _height,),
              // Popular text
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "Most Popular",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: _height,),
              Container(
                width: MediaQuery.of(context).size.width,
                child: FutureBuilder<List<FoodModel>>(
                    future: _foodModel,
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        // suppose to check for the meal with the highest rating and return it
                        // for(int i = 0; i < snapshot.data.length; i++ ){
                        //
                        // }
                        return FoodCard(snapshot.data[0]);
                      }else{
                        return Center(
                          child: Text("Loading..."),
                        );
                      }

                    }

                ),
              ),

            ],
          ),
        ),],
      ),

    );
  }
  void filterCategory(String input){
  item =  _categories.where((item) {
     return item.imageTitle.toLowerCase()
              .startsWith(input.toLowerCase());
   }).toList();
   if(item.isNotEmpty){
     print(item);
     setState(() {
       _categories = item;
       item = [];
     });
   }else{
     print(temp);
     setState(() {
       _categories = temp;
     });
   }
  }
}
