import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {

  final TextStyle _textStyle = TextStyle(
    // styles for the welcoming text
    // fontFamily still to be added
    color: Colors.white,
    fontSize: 40,
    fontWeight: FontWeight.bold,
    decoration: TextDecoration.none,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      //alignment: AlignmentDirectional.bottomEnd,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [Colors.pink[600], Colors.pinkAccent[100]]
        ),
      ),
      child: Stack(
        children: [
          Center(
            child: Text(
                "Cooking Idea",
              style: _textStyle,
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              alignment: AlignmentDirectional.bottomEnd,
              width: 250,
              height: 150,
              // An asset image imported from the assets folder in the root directory
              child: Image(
                image: AssetImage('assets/veg-1.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
