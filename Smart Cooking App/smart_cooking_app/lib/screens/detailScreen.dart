import 'package:flutter/material.dart';
import 'package:smart_cooking_app/componentWidgets/ingredientCard.dart';
import 'package:smart_cooking_app/models/foodModel.dart';

class DetailScreen extends StatefulWidget {
  final FoodModel food;
  DetailScreen(this.food, {Key key}):super(key: key);
  @override
  _DetailScreenState createState() => _DetailScreenState();
}


class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
          ),
        ),
        title: Text(
          "Details",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(5,5,5,0),
        child: ListView(
          children: [
            Container(
              color: Colors.black12,
              height: MediaQuery.of(context).size.width/1.1,
              child: Hero(
                tag: "${widget.food.image}",
                child: Image(
                  image: NetworkImage(widget.food.image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.food.title.length > 15 ? widget.food.title.substring(0,15) : widget.food.title,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 50,
                  child: Row(
                    children: [
                      Icon(
                          Icons.star,
                        size: 16,
                        color: Colors.yellow,
                      ),
                      SizedBox(width: 5,),
                      Text("${widget.food.likes}",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),)
                    ],
                  ),
                ),
              ],
            ),
            Divider(
              height: 20,
              thickness: 2,
              color: Colors.grey[300],
            ),
            Text(
              "Description",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 5,),
            Container(
              child: Text(
                "The basic spaghetti recipe made with fresh tomatoes and chicken. This"
                    "This is quick to make for a meal on a busy day as it doesn't take much time"
                    "veggies, protein and grain all go in one dish making it a healthy and wholesome one without much effort.",
                style: TextStyle(
                  fontSize: 18,

                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
