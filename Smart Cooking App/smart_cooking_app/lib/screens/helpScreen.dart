import 'package:flutter/material.dart';

class Help extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
                Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: (){
              Navigator.pop(context);
            }
        ),
        title: Text("Help"),
      ),
      body: Container(
        child: Text(
          "- Click the menu button to navigate to the input ingredient"
              "-Enter a list of ingredients and submit"
              "- A list of recipes will be provided"
        ),
      ),
    );
  }
}
