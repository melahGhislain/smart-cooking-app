import "package:http/http.dart" as http;
import 'package:smart_cooking_app/models/foodModel.dart';

class Data{
  final String ingredients;

  Data({this.ingredients,});

  Future<List<FoodModel>>  getFood() async{
        var foodModel = null;
        var client = http.Client();
        try{
          final response = await client.get('https://api.spoonacular.com/recipes/findByIngredients?apiKey=861169aa2a144fc0a19e72a0696419ca&ingredients=$ingredients');
          if(response.statusCode == 200){
            var jsonString  = response.body;

            foodModel = foodModelFromJson(jsonString);
            //print(foodModel);
          }
        }catch(e){
          print("An error occured: $e");
          return foodModel;
        }

      return foodModel;

  }
//https://api.spoonacular.com/recipes/findByIngredients?apiKey=861169aa2a144fc0a19e72a0696419ca&ingredients=meat,+rice,+tomatoes


}