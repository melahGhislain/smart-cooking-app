class Food{
  final String imageUrl;
  final String imageTitle;
  final String description;
  final String category;

  Food({this.imageUrl, this.imageTitle, this.description, this.category});
}