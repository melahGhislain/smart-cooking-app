
import 'dart:convert';

List<FoodModel> foodModelFromJson(String str) => List<FoodModel>.from(json.decode(str).map((x) => FoodModel.fromJson(x)));

String foodModelToJson(List<FoodModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FoodModel {
  FoodModel({
    this.id,
    this.title,
    this.image,
    this.imageType,
    this.usedIngredientCount,
    this.missedIngredientCount,
    this.missedIngredients,
    this.usedIngredients,
    this.unusedIngredients,
    this.likes,
  });

  int id;
  String title;
  String image;
  ImageType imageType;
  int usedIngredientCount;
  int missedIngredientCount;
  List<SedIngredient> missedIngredients;
  List<SedIngredient> usedIngredients;
  List<SedIngredient> unusedIngredients;
  int likes;

  factory FoodModel.fromJson(Map<String, dynamic> json) => FoodModel(
    id: json["id"],
    title: json["title"],
    image: json["image"],
    imageType: imageTypeValues.map[json["imageType"]],
    usedIngredientCount: json["usedIngredientCount"],
    missedIngredientCount: json["missedIngredientCount"],
    missedIngredients: List<SedIngredient>.from(json["missedIngredients"].map((x) => SedIngredient.fromJson(x))),
    usedIngredients: List<SedIngredient>.from(json["usedIngredients"].map((x) => SedIngredient.fromJson(x))),
    unusedIngredients: List<SedIngredient>.from(json["unusedIngredients"].map((x) => SedIngredient.fromJson(x))),
    likes: json["likes"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "image": image,
    "imageType": imageTypeValues.reverse[imageType],
    "usedIngredientCount": usedIngredientCount,
    "missedIngredientCount": missedIngredientCount,
    "missedIngredients": List<dynamic>.from(missedIngredients.map((x) => x.toJson())),
    "usedIngredients": List<dynamic>.from(usedIngredients.map((x) => x.toJson())),
    "unusedIngredients": List<dynamic>.from(unusedIngredients.map((x) => x.toJson())),
    "likes": likes,
  };
}

enum ImageType { JPG }

final imageTypeValues = EnumValues({
  "jpg": ImageType.JPG
});

class SedIngredient {
  SedIngredient({
    this.id,
    this.amount,
    this.unit,
    this.unitLong,
    this.unitShort,
    this.aisle,
    this.name,
    this.original,
    this.originalString,
    this.originalName,
    this.metaInformation,
    this.meta,
    this.image,
    this.extendedName,
  });

  int id;
  double amount;
  String unit;
  String unitLong;
  String unitShort;
  String aisle;
  String name;
  String original;
  String originalString;
  String originalName;
  List<String> metaInformation;
  List<String> meta;
  String image;
  String extendedName;

  factory SedIngredient.fromJson(Map<String, dynamic> json) => SedIngredient(
    id: json["id"],
    amount: json["amount"].toDouble(),
    unit: json["unit"],
    unitLong: json["unitLong"],
    unitShort: json["unitShort"],
    aisle: json["aisle"] == null ? null : json["aisle"],
    name: json["name"],
    original: json["original"],
    originalString: json["originalString"],
    originalName: json["originalName"],
    metaInformation: List<String>.from(json["metaInformation"].map((x) => x)),
    meta: List<String>.from(json["meta"].map((x) => x)),
    image: json["image"],
    extendedName: json["extendedName"] == null ? null : json["extendedName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "amount": amount,
    "unit": unit,
    "unitLong": unitLong,
    "unitShort": unitShort,
    "aisle": aisle == null ? null : aisle,
    "name": name,
    "original": original,
    "originalString": originalString,
    "originalName": originalName,
    "metaInformation": List<dynamic>.from(metaInformation.map((x) => x)),
    "meta": List<dynamic>.from(meta.map((x) => x)),
    "image": image,
    "extendedName": extendedName == null ? null : extendedName,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
