class Category{
  final int id;
  final String imageUrl;
  final String imageTitle;

  Category({this.id, this.imageUrl, this.imageTitle});

}